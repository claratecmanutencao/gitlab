# frozen_string_literal: true

module QA
  # run only base UI validation on staging because test requires top level group creation which is problematic
  # on staging environment
  RSpec.describe 'Manage', :requires_admin, except: { subdomain: :staging } do
    describe 'Gitlab migration' do
      let(:import_wait_duration) { { max_duration: 300, sleep_interval: 2 } }
      let(:admin_api_client) { Runtime::API::Client.as_admin }
      let(:user) do
        Resource::User.fabricate_via_api! do |usr|
          usr.api_client = admin_api_client
          usr.hard_delete_on_api_removal = true
        end
      end

      let(:api_client) { Runtime::API::Client.new(user: user) }

      let(:sandbox) do
        Resource::Sandbox.fabricate_via_api! do |group|
          group.api_client = admin_api_client
        end
      end

      let(:source_group) do
        Resource::Sandbox.fabricate_via_api! do |group|
          group.api_client = api_client
          group.path = "source-group-for-import-#{SecureRandom.hex(4)}"
        end
      end

      let(:source_project) do
        Resource::Project.fabricate_via_api! do |project|
          project.api_client = api_client
          project.group = source_group
          project.initialize_with_readme = true
        end
      end

      let(:imported_group) do
        Resource::BulkImportGroup.fabricate_via_api! do |group|
          group.api_client = api_client
          group.sandbox = sandbox
          group.source_group_path = source_group.path
        end
      end

      let(:imported_projects) do
        imported_group.reload!.projects
      end

      let(:import_failures) do
        imported_group.import_details.sum([]) { |details| details[:failures] }
      end

      before do
        Runtime::Feature.enable(:bulk_import_projects)

        sandbox.add_member(user, Resource::Members::AccessLevel::MAINTAINER)

        source_project.tap { |project| project.add_push_rules(member_check: true) } # fabricate source group and project
      end

      after do |example|
        # Checking for failures in the test currently makes test very flaky
        # Just log in case of failure until cause of network errors is found
        Runtime::Logger.warn("Import failures: #{import_failures}") if example.exception && !import_failures.empty?

        user.remove_via_api!
      ensure
        Runtime::Feature.disable(:bulk_import_projects)
      end

      context 'with project' do
        before do
          imported_group # trigger import
        end

        it(
          'successfully imports project',
          testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347610'
        ) do
          expect { imported_group.import_status }.to eventually_eq('finished').within(import_wait_duration)
          expect(imported_projects.count).to eq(1), 'Expected to have 1 imported project'
          expect(imported_projects.first).to eq(source_project)
        end
      end

      context 'with project issues' do
        let(:source_issue) do
          Resource::Issue.fabricate_via_api! do |issue|
            issue.api_client = api_client
            issue.project = source_project
            issue.labels = %w[label_one label_two]
          end
        end

        let(:imported_issues) do
          imported_projects.first.issues
        end

        let(:imported_issue) do
          issue = imported_issues.first
          Resource::Issue.init do |resource|
            resource.api_client = api_client
            resource.project = imported_projects.first
            resource.iid = issue[:iid]
          end
        end

        before do
          source_issue # fabricate source group, project, issue
          imported_group # trigger import
        end

        it(
          'successfully imports issue',
          testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347608'
        ) do
          expect { imported_group.import_status }.to eventually_eq('finished').within(import_wait_duration)
          expect(imported_projects.count).to eq(1), 'Expected to have 1 imported project'

          aggregate_failures do
            expect(imported_issues.count).to eq(1)
            expect(imported_issue.reload!).to eq(source_issue)
          end
        end
      end

      context 'with repository' do
        let(:imported_project) { imported_projects.first }

        let(:source_commits) { source_project.commits.map { |c| c.except(:web_url) } }
        let(:source_tags) do
          source_project.repository_tags.tap do |tags|
            tags.each { |t| t[:commit].delete(:web_url) }
          end
        end

        let(:source_branches) do
          source_project.repository_branches.tap do |branches|
            branches.each do |b|
              b.delete(:web_url)
              b[:commit].delete(:web_url)
            end
          end
        end

        let(:imported_commits) { imported_project.commits.map { |c| c.except(:web_url) } }
        let(:imported_tags) do
          imported_project.repository_tags.tap do |tags|
            tags.each { |t| t[:commit].delete(:web_url) }
          end
        end

        let(:imported_branches) do
          imported_project.repository_branches.tap do |branches|
            branches.each do |b|
              b.delete(:web_url)
              b[:commit].delete(:web_url)
            end
          end
        end

        before do
          source_project.create_repository_branch('test-branch')
          source_project.create_repository_tag('v0.0.1')
          imported_group # trigger import
        end

        it(
          'successfully imports repository',
          testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347570'
        ) do
          expect { imported_group.import_status }.to eventually_eq('finished').within(import_wait_duration)
          expect(imported_projects.count).to eq(1), 'Expected to have 1 imported project'

          aggregate_failures do
            expect(imported_commits).to match_array(source_commits)
            expect(imported_tags).to match_array(source_tags)
            expect(imported_branches).to match_array(source_branches)
          end
        end
      end

      context 'with wiki' do
        before do
          source_project.create_wiki_page(title: 'Import test project wiki', content: 'Wiki content')
          imported_group # trigger import
        end

        it(
          'successfully imports project wiki',
          testcase: 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/347567'
        ) do
          expect { imported_group.import_status }.to eventually_eq('finished').within(import_wait_duration)
          expect(imported_projects.count).to eq(1), 'Expected to have 1 imported project'
          expect(imported_projects.first.wikis).to eq(source_project.wikis)
        end
      end
    end
  end
end
